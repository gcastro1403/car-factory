
public class BuilderCar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BaseCar luxuryCar = CarFactory.buildCar(TypeCar.LUXURY);
		luxuryCar.construct();
		
		BaseCar sedanCar = CarFactory.buildCar(TypeCar.SEDAN);
		sedanCar.construct();
		
		BaseCar smallCar = CarFactory.buildCar(TypeCar.SMALL);
		smallCar.construct();

	}

}
