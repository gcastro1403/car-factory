
public class CarFactory {
	
	
	public static BaseCar buildCar(TypeCar typeCar) {
		
		switch(typeCar) {
			case SEDAN:
				return new SedanCar();
			case LUXURY:
				return new LuxuryCar();
			case SMALL:
				return new SmallCar();
			default:
				return new SmallCar();
		}
		
	}

}
